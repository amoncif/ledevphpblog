<?php

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Cocur\Slugify\Slugify;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $userPasswordEncoder;

    /** @var Factory  */
    private $faker;

    public function __construct(UserPasswordEncoderInterface $userPasswordEncoder)
    {
        $this->userPasswordEncoder = $userPasswordEncoder;
        $this->faker = Factory::create();

    }

    public function load(ObjectManager $manager)
    {
        $this->loadUsers($manager);
        $this->loadBlogPosts($manager);
        $this->loadComments($manager);
    }

    public function loadUsers(ObjectManager $manager)
    {
        $user = (new User())
            ->setUsername("admin")
            ->setEmail("admin@amoncif.site")
            ->setName("Moncif AISSAOUI");
        $user->setPassword($this->userPasswordEncoder->encodePassword($user, "joury@2310"));

        $this->addReference('user_admin', $user);
        $manager->persist($user);
        $manager->flush();
    }


    public function loadBlogPosts(ObjectManager $manager)
    {
        $author = $this->getReference('user_admin');

        for ($i = 0; $i < 50 ; $i++) {
            $blogPost = (new BlogPost())
               ->setTitle($this->faker->realText(30))
               ->setContent($this->faker->realText())
               ->setPublished($this->faker->dateTime)
               ->setAuthor($author);
            $blogPost->setSlug((new Slugify())->slugify($blogPost->getTitle()));

            $manager->persist($blogPost);
        }
        $manager->flush();
    }

    public function loadComments(ObjectManager $manager)
    {
        $author = $this->getReference('user_admin');

        $blogPosts = $manager->getRepository(BlogPost::class)->findAll();

        foreach ($blogPosts as $blogPost) {
            for($j = 0; $j < rand(1,10); $j++) {
                $comment = (new Comment())
                    ->setContext($this->faker->realText())
                    ->setPublished($this->faker->dateTimeThisYear)
                    ->setBlogPost($blogPost)
                    ->setAuthor($author);
                $manager->persist($comment);
            }
        }
        $manager->flush();
    }
}
